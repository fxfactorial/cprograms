#include <stdio.h>

int main(void){

  int counter = 'A';
  while(counter <= 'Z'){
    printf("%c", counter++);
  }

  counter = 'a';

  printf("\n");
  while(counter <= 'z'){
    printf("%c", counter++);
  }
  printf("\n");
  return 0;
}
