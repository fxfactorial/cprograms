#include <stdio.h>

double areaOfRectangle(double a, double b);

int main(void){

  double area;

  printf("The area of a rectangle is %.4f\n", areaOfRectangle(14.2, 15.9));

  return 0;
}

double areaOfRectangle(double a, double b){

  return a*b;
}
