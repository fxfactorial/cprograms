#include <stdio.h>

int main(void){

  int integer = 8080;
  int *pointer = &integer;

   printf("integer = %d, *pointer = %d\n", integer, *pointer);

    *pointer = 8086;

    printf("integer = %d, *pointer = %d\n", integer, *pointer);

   (*pointer)++;
  printf("*pointer is %d\n", *pointer);

  ++(*pointer);

  printf("what's the pointer now? %d\n", *pointer);

  return 0;
}
