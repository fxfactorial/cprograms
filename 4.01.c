#include <stdio.h>

int main(void){

  int true = 1;
  int false = 0;

  if(true){
    printf("%d is true\n", true);
  }
  true++;
  if(true){
    printf("%d is true\n", true);
  }
  if(false){
    printf("%d is true\n", false);
  }
  false--;
  if(false){
    printf("%d is true\n", false);
  }
  return 0;
}
