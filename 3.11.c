#include <stdio.h>

int main(void){

  int int1 = 5;
  int int2 = '5';
  printf("The value of int1 is %d\n", int1);
  printf("and the value of int2 is %d\n", int2);
  printf("but the ASCII character for int2 is %c\n", int2);

  return 0;
}
