#include <stdio.h>

void displayNums(int nums[], int SIZE);

int main(void){

  int nums[] = {11, 13, 17, 19, 23, 29};
  int SIZE = sizeof(nums)/sizeof(nums[0]);
  displayNums(nums, SIZE);

  return 0;
}

void displayNums(int nums[], int SIZE){
  int i;
  for(i = 0; i < SIZE; i++){
    printf("%d\t", nums[i]);
  }
  printf("\n");
}
