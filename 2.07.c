#include <stdio.h>
#define SYMBOLIC_CONSTANT 177

int main(void){
  int variable = 249;
  printf("This is a literal constant: %d\n", 287);
  printf("This is a symbolic constant: %d\n", SYMBOLIC_CONSTANT);
  printf("this is a variable: %d\n", variable);
  int variable2;
  variable2 = SYMBOLIC_CONSTANT;
  printf("This is the new assigned variable %d", variable2);


  return 0;
}
