#include <stdio.h>

int main(void){

  int i;
  int *j;
  //  j = &i;

  printf("i is an integer is its size is %zu, while *j is a pointer and its size is %zu\n", sizeof(i), sizeof(*j));

  printf("i's memory address is %p, while *j's memory address is %p\n", &i, &*j);

  return 0;
}
