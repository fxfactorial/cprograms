#include <stdio.h>

int func1(int a);
int func2(int a);

int main(void){

  printf("func1 address = %p\n", func1);
  //printf("func1 address = %p\n", func1);

  printf("func2 address = %p\n", func2);
  //printf("func2 address = %p\n", &func2);

  return 0;
}

int func1(int a){

  return 1;
}

int func2(int a){

  return 1;
}
