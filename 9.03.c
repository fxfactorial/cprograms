#include <stdio.h>

void myPuts(char *p);

int main(void){

  myPuts("An elephant is a mouse with an operating system");
  myPuts("Want some rye? 'Course ya do!");

  return 0;
}

void myPuts(char *p){

  while(*p){
    printf("[%c]", *p++);
  }
  printf("\n");
}
