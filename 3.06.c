#include <stdio.h>

int main(void){

  char letter1 = 7;
  char letter2 = 61;
  char letter3 = 37;
  printf("%c %c %c\n", 'a', 'b', 'c');
  printf("%c\t%c\t%c\n", letter1, letter2, letter3);
  printf("The memory locations are %p, %p, %p\n", &letter1, &letter2, &letter3);

  return 0;
} 
