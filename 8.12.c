#include <stdio.h>

int main(void){

  char *alpha, character;
  unsigned alphaRValue, characterLValue;
  character = 's';

  alpha = &character;

  printf("The contents of the pointer 'alpha' = %c\n", *alpha);

  printf("The lvalue of the pointer = %p\n", &alpha);
  printf("The rvalue of the pointer = %p\n", alpha);
  printf("lvalue of the character = %p\n", &character);
  printf("rvalue of the character = %p\n", character);

  alphaRValue = (unsigned)alpha;
  characterLValue = (unsigned)&character;
  if(alphaRValue == characterLValue){
    printf("\nThe rvalue of the pointer is the same\n");
    printf("as the lvalue of the data item it points to\n");
  }
  return 0;
} 
