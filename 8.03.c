#include <stdio.h>

int main(void){

  int i;
  int *j;

  double f;
  double *g;

  char c, *d;

  j = &i;
  printf("j = %p \t&i = %p\n", j, &i);

  g = &f;
  printf("g = %p \t&f = %p\n", g, &f);

  d = &c;
  printf("d = %p \t&c = %p\n", d, &c);

  return 0;
}
