#include <stdio.h>

int main(void){

  int var = 2;
  while(var){
    printf("var (%d) is true\n", var--);
  }
  printf("var (%d) is now false \n\n", var);
  printf("Exiting loop. \n");

  var = -2;

  while(var){
    printf("var (%d) is now true\n", var++);
  }
  printf("var (%d) is now false\n", var);
  printf("Exiting loop.\n");

  return 0;
}
