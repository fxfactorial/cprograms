#include <stdio.h>

void function1(int i);
int function2(int i);

int main(void){

  int i = 73;
  int j = 42;
  printf("\nIn the main function:\n The value of i is %d and its address is %p\n", i, &i);

  printf("the value of j is %d and its address is %p\n\n", j, &j);

  function1(i);
  printf("The value of j is %d and its address is %p\n", j, &j);
  j = function2(i);

  printf("the value of j is now %d and its address is %p\n", j, &j);

  return 0;

}

void function1(int i){

  i +=i;
}

int function2(int i){

  return i +=3;
}
