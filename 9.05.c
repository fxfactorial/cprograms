#include <stdio.h>

void byValue(int iNum, double fNum);
void byReference(int *iNumPtr, double *fNumPtr);

int main(void){

  int iNum = 4075;
  double fNum = 9.1839;

  printf("Within the main() function:\t , also the main function is at %p", &main);

  printf("iNum = %d, fNum = %f\n", iNum, fNum);
  byValue(iNum, fNum);
  printf("\nWithin the main() function:\t ");
  printf("iNum = %d, fNum = %f\n", iNum, fNum);

  byReference(&iNum, &fNum);
  printf("\nWithin the main() function:\t ");
  printf("iNum = %d, fNum = %f\n", iNum, fNum);

  return 0;
}

void byValue(int iNum, double fNum){

  iNum = 196;
  fNum = 5.951;
  printf("\nWithin the byValue() function:\t iNum = %d, fNum = %f\n", iNum, fNum);
}

void byReference(int *iNumPtr, double *fNumPtr){

  *iNumPtr = 399;
  *fNumPtr = 19.995;
  printf("\nWithin the byReference() function:\t *iNumptr = %d, *fNumPTr = %f\n and the actual byReference function is at %p", *iNumPtr, *fNumPtr, &byReference);
}
