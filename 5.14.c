#include <stdio.h>
#include <stdlib.h>

int main(void){

  int i;
  printf("Here is a list of pseudo random numbers\n\n");
  for(i = 0; i < 50; i++){
    if(i != 0 && i % 5 == 0){
      printf("\n");
    }
    printf("%10d\t", rand());
  }
  printf("\n\n");
  return 0;
}
