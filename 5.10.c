#include <stdio.h>

int main(void){
  int count;
  int limit= 10;
  printf("----Before Loop----\n");
  for(count = 1; count <= limit; count++){
    printf("Pass no. %d\n", count);
  }
  printf("----After Loop----\n");
  return 0;
}
