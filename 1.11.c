#include <stdio.h>

int main(void){

  printf("%s\n", "This\t is\t a\t string\t literal\t.");
  printf("%40s\n", "This is also a string literal");
  printf("%-40s\n", "This is also a string literal.");

  printf("%3c %3c %3c\n", 'x', 'y', 'z');
  printf("%-3c %-3c %-3c\n", 'a', 'b', 'c');

  printf("Five prime numbers: %d %d %d %d %d\n", 1+2, 4+1, 4+3, 6+5, 7+6);

  return 0;
}
