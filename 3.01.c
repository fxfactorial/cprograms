#include <stdio.h>

int main(void){

  int intVal = 3;
  float floatVal = 3.14;

  printf("The value of the integer variable is %d\n", intVal);
  printf("The value of the float variable is %f\n", floatVal);

  return 0;
}
