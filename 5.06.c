#include <stdio.h>

int main(void){

  int num = 100;
  int denom = 2;
  while(denom < num){
    if(num % denom == 0){
      printf("%d is divisible by %d\n", num, denom);
    }
    denom++;
  }
    return 0;
}
