#include <stdio.h>

int main(void){
  int n = -5;
  printf("The value of n is %d\n", n);
  if(n < 0){
    n = -n;
  }
  printf("The absolute value of n is %d\n", n);
  return 0;
}
