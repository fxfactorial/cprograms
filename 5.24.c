#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void){

  int guess, number;
  number = (rand()*(unsigned)time(NULL))% 10000;
  guess = 0;
  while(1){
    if(guess++!=number){
      printf("still searching...\n");
    }
    else{
      printf("%d == %d\n", guess-1, number);
      break;
    }
