#include <stdio.h>

int main(void){

  int color = 0xFFFFFF;
  switch(color){
  case 0x000000:
    printf("Black\n");
    break;
  case 0xFFFFFF:
    printf("White!\n");
    break;
  case 0x0000CC:
    printf("Blue!\n");
    break;
  default:
    printf("We do not have that color.\n");
    break;
  }
  return 0;
}
