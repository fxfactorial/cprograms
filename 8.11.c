#include <stdio.h>

int main(void){

  double dbArray[] = {10.5, 11.1, 12.11, 13.121, 14.1312};

  double *dbPtr;

  printf("Doubles need %lu bytes on this machine\n", sizeof(dbArray[0]));

  dbPtr = dbArray;

  printf("%f\n", *dbPtr);


  return 0;

}
